# Prudence build from sources

Goal: 
- Build prudence without any dependencies from the threecricket repository (http://repository.threecrickets.com/maven/)
- Use gitlab to build threecricket artifacts from source and deploy them to artifactory


### Projects to build from source:

- json-jvm
- creel
- scripturian
- succinct
- jygments
- mongodb-jvm
- sincerity
- prudence
- restlet-jetty9

### Building threecricket artifacts from source

To process for building any of the threecricket artifacts is, in theory, as follows:

1. Use the custom made docker image `notterservice/ant-mvn-git:latest` for the gitlab job
1. Clone the git repository you want to build from source
1. `cd` into the `build` folder, e.g. `creel/build` and run `ant deploy-maven`

In practice, a lot of the repositories required fixes as the builds were broken.

I have forked all the repositories and fixed the builds:

- https://github.com/enpayne/json-jvm.git
- https://github.com/enpayne/creel.git
- https://github.com/enpayne/scripturian.git
- https://github.com/enpayne/succinct.git
- https://github.com/enpayne/jygments.git
- https://github.com/enpayne/mongodb-jvm.git
- https://github.com/enpayne/sincerity.git
- https://github.com/enpayne/prudence.git
- https://github.com/enpayne/restlet-jetty9.git

Now, there were **a lot** of issues with the sincerity build. It has many dependencies which don't exist anymore (except on the threecricket repository),
and many of them I could not even find on github or elsewhere to see if they could be built from source as well. I have downloaded the missing jars from the 
threecricket repository directly and put them inside the repo, maven will use those directly. So if you look at my commit on the prudence repo, you will see
which jars need to be added to the artifactory, if you want to get rid of the jars in the repository.

As discussed, I have also removed all the skeletons as most of them were missing dependencies as well and you do not need them anyway. Should you need one in 
the future, you would have to add the missing jars to your artifactory as wel.

It is necessary to add `org.zkoss.zuss:zuss:1.0.0` to your repository, because creel cannot be configured to pull local dependencies like maven does.

### Deploying to artifactory

The artifactory uri can be defined directly in the `.gitlab-ci.yml` or a secret can be created called `ARTIFACTORY_URI`. Gitlab will then pass that to ant.
This property will then be used by creel to resolve dependencies and maven will use `central` by default.

If you want maven to fetch dependencies from your artifactory as well, it will be necessary to add a `settings.xml` containing your
artifactory details to the builder image (you could do this inside the gitlab build, for example).

### Testing locally

To test locally, you can use docker to start an artifactory OSS instance and a container with the 
builder image used for gitlab.

Run from the root of this project:

`docker-compose up`

Artifactory is available on `localhost:8081`. You need to set up a maven repository there that you can deploy to.

Inside the builder container, you have `git` installed so you can use that to clone the repos and run

`ant -Ddistribution.repo=$ARTIFACTORY_URI -Ddependencies.repo=$ARTIFACTORY_URI deploy-maven`

like gitlab would do, and watch what happens.